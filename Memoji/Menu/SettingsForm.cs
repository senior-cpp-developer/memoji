﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji.Menu
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();

            numInit.Value = Data.settings.cardsVisibInit;
            numSel.Value = Data.settings.cardsVisibReverse;
            numPairs.Value = Data.settings.NoCards / 2;
            textUsername.Text = Data.settings.userName;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Data.settings.cardsVisibInit = (int)numInit.Value;
            Data.settings.cardsVisibReverse = (int)numSel.Value;
            Data.settings.NoCards = (int)numPairs.Value * 2;
            Data.settings.userName = textUsername.Text;
            Data.settings.saveSettings();
            this.Close();
        }
    }
}
