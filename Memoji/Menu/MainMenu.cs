﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            FormsController.newGame();
        }

        private void buttonLeaderboard_Click(object sender, EventArgs e)
        {
            FormsController.leaderboard();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            FormsController.settings();
        }
    }
}
