﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji.Menu
{
    public partial class Leaderboard : Form
    {
        void refreshLeaderboard()
        {
            list.Items.Clear();
            foreach (Result r in Data.game.leaderboard)
                list.Items.Add(new ListViewItem(r.score + "   " + r.userName));
        }

        void newScore(int score)
        {
            textScore.Text = "🏆 Your score: " + score;
            tableCongratz.Visible = true;
            Data.addResult(score);
            refreshLeaderboard();
        }
    }
}
