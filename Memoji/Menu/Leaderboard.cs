﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji.Menu
{
    public partial class Leaderboard : Form
    {
        public Leaderboard()
        {
            InitializeComponent();
            refreshLeaderboard();
        }

        public Leaderboard(int score) : this()
        {
            newScore(score);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
