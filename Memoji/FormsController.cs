﻿using Memoji.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memoji
{
    internal static class FormsController
    {
        public static MainMenu mainMenuForm;
        public static Game gameForm;
        public static Leaderboard leaderboardForm;
        public static SettingsForm settingsForm;

        public static void mainMenu()
        {
            mainMenuForm.Show();
        }

        public static void newGame()
        {
            closeAll();
            gameForm = new Game();
            gameForm.Show();
        }

        public static void leaderboard()
        {
            leaderboardForm?.Close();
            leaderboardForm = new Leaderboard();
            leaderboardForm.Show();
        }

        public static void leaderboard(int score)
        {
            leaderboardForm?.Close();
            leaderboardForm = new Leaderboard(score);
            leaderboardForm.Show();
        }

        public static void settings()
        {
            settingsForm?.Close();
            settingsForm = new SettingsForm();
            settingsForm.Show();

        }

        static void closeAll()
        {
            mainMenuForm?.Hide();
            gameForm?.Close();
        }
    }
}
