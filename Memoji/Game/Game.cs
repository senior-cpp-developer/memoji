﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji
{
    public partial class Game : Form
    {
        public Game()
        {
            InitializeComponent();
        }

        private void Game_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormsController.mainMenu();
        }

        private void Game_Load(object sender, EventArgs e)
        {
            initGame();
        }

        private void timerReverse_Tick(object sender, EventArgs e)
        {
            reverseCardsHandler();
            Timer t = (Timer)sender;
            t.Enabled = false;
        }

        private void cardExample_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            cardClickHandler(b);
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            pauseGameHandler();
        }

        private void timerGame_Tick(object sender, EventArgs e)
        {
            gameTickHandler();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            FormsController.settings();
        }
    }
}
