﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memoji
{
    public partial class Game : Form
    {
        Button clickedCard = null;
        List<string> emojis = new List<string>();
        int elapsedTime = 0;
        bool isPaused = false;
        int tries = 0;

        public void initGame()
        {
            createBoard();
            timerGame.Enabled = true;
            timerReverse.Interval = Data.settings.cardsVisibInit * 1000;
            timerReverse.Enabled = true;
        }

        void createBoard()
        {
            int noCards = Data.settings.NoCards;

            int noEmojis = typeof(J3QQ4.Emoji).GetFields().Length;
            //var emoji = emojis[50].GetValue(null);

            Random r = new Random();
            List<int> numbers = new List<int>();
            for (int i = 0; i < noCards / 2; i++)
            {
                while (true)
                {
                    int t = r.Next(0, noEmojis - 1);
                    if (!numbers.Contains(t))
                    {
                        numbers.Add(t);
                        break;
                    }
                }
            }

            int len = numbers.Count;
            for (int i = 0; i < len; i++)
                numbers.Add(numbers[i]);
            numbers = numbers.OrderBy(x => r.Next()).ToList();

            createButtons(numbers);
        }

        void createButtons(List<int> numbers)
        {
            var allEmojis = typeof(J3QQ4.Emoji).GetFields();
            Button e = cardExample;

            foreach (int i in numbers)
            {
                Button b = new Button();
                b.Font = e.Font;
                b.Size = e.Size;
                b.Text = (string)allEmojis[i].GetValue(null);
                b.Enabled = false;
                b.Click += new System.EventHandler(this.cardExample_Click);
                board.Controls.Add(b);

                emojis.Add((string)allEmojis[i].GetValue(null));
            }
        }

        public void cardClickHandler(Button caller)
        {
            caller.Text = emojis[board.Controls.IndexOf(caller) - 1];

            if (clickedCard == null)
            {
                shownCards(true);
                caller.Enabled = false;
                clickedCard = caller;
            }
            else
            {
                disableCards();
                addTry();
                timerReverse.Interval = Data.settings.cardsVisibReverse * 1000;
                timerReverse.Enabled = true;

                if (clickedCard.Text == caller.Text)
                {
                    clickedCard.Text = "";
                    caller.Text = "";
                }
                checkForWin();
            }
        }

        void shownCards(bool areShown)
        {
            buttonPause.Enabled = !areShown;
        }

        void reverseCardsHandler()
        {
            foreach (Button b in board.Controls)
            {
                if (b.Text != "")
                {
                    b.Text = "?";
                    b.Enabled = true;
                }
                    
            }
            clickedCard = null;
            shownCards(false);
        }

        void disableCards()
        {
            foreach (Button b in board.Controls)
                b.Enabled = false;
        }

        void pauseGameHandler()
        {
            if (isPaused)
            {
                buttonPause.Text = "⏸️  Pause";
                buttonSettings.Enabled = false;
                isPaused = false;
                timerGame.Enabled = true;
                reverseCardsHandler();
            }
            else
            {
                buttonPause.Text = "⏏️  Play";
                buttonSettings.Enabled = true;
                isPaused = true;
                timerGame.Enabled = false;
                disableCards();
            }

        }

        void gameTickHandler()
        {
            elapsedTime++;
            textTime.Text = (elapsedTime / 60) + ":" 
                + (((elapsedTime % 60) < 10) ? "0" : "") + (elapsedTime % 60) ;
        }

        void addTry()
        {
            tries++;
            textTries.Text = tries + " tries";
        }

        void checkForWin()
        {
            foreach (Button b in board.Controls)
                if (b.Text != "")
                    return;
            int score = Data.settings.NoCards * 1000 - tries * 100 - elapsedTime * 10;
            FormsController.leaderboard(score);
            this.Close();
        }
    }
}
