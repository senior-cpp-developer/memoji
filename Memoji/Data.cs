﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.IO;
using System.Text.Json.Serialization;

namespace Memoji
{
    internal class Result
    {
        public Result(int score, string userName)
        {
            this.score = score;
            this.userName = userName;
        }
        [JsonInclude]
        public int score;
        [JsonInclude]
        public string userName;
    }

    internal class Settings
    {
        static string fileName = "Settings.json";

        [JsonInclude]
        public int cardsVisibInit;
        [JsonInclude]
        public int cardsVisibReverse;
        int noCards;
        public int NoCards
        {
            get { return noCards; }
            set
            {
                if (noCards % 2 != 0)
                    throw new ArgumentException();
                noCards = value;
            }
        }
        [JsonInclude]
        public string userName;

        public Settings()
        {
            cardsVisibInit = 7;
            cardsVisibReverse = 1;
            noCards = 4;
            userName = Environment.UserName;
        }

        public void saveSettings()
        {
            string jsonString = JsonSerializer.Serialize(this);
            File.WriteAllText(fileName, jsonString);
        }

        public static Settings loadSettings()
        {
            if (File.Exists(fileName))
            {
                string jsonString = File.ReadAllText(fileName);
                return JsonSerializer.Deserialize<Settings>(jsonString);
            }
            else return new Settings();
        }
    }

    internal class GameData
    {
        static string fileName = "Data.json";

        [JsonInclude]
        public List<Result> leaderboard = new List<Result>();

        public void sortLeaderboard()
        {
            leaderboard.Sort((x, y) => y.score.CompareTo(x.score));
        }

        public void addResult(Result result)
        {
            leaderboard.Add(result);
            sortLeaderboard();
        }

        public static GameData load()
        {
            if (File.Exists(fileName))
            {
                string jsonString = File.ReadAllText(fileName);
                return JsonSerializer.Deserialize<GameData>(jsonString);
            }
            else return new GameData();
        }

        public void save()
        {
            string jsonString = JsonSerializer.Serialize(this);
            File.WriteAllText(fileName, jsonString);
        }
    }

    internal static class Data
    {
        public static GameData game = GameData.load();
        public static Settings settings = Settings.loadSettings();

        public static void addResult(int score)
        {
            game.addResult(new Result(score, settings.userName));
            game.save();
        }
    }
}
